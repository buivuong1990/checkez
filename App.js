import React, {Component} from 'react';
import {View} from 'react-native';

import { NativeRouter, Route } from "react-router-native";

import CheckDevice from "./src-ios";
import Confirm from './src-ios/confirm';
import Transaction from "./src-ios/transaction";
import Thanks from "./src-ios/thanks";

export default class App extends Component{
  constructor(props){
    super(props);
  }
  render() {
    return (
        <NativeRouter>
          <View>
            <Route exact path="/" component={CheckDevice}/>
            <Route exact path="/confirm/:number" component={Confirm}/>
            <Route exact path="/transaction" component={Transaction}/>
            <Route exact path="/thanks/:number" component={Thanks}/>
          </View>
        </NativeRouter>
    );
  }
}