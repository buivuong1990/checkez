import * as React from 'react';
import { Text, View, ScrollView, StyleSheet,ImageBackground,TouchableOpacity,Image,Dimensions} from 'react-native';
import CapitalizedText from "./capitalizedText";

import {NativeModules} from 'react-native';
const PackageManagerNative = NativeModules.PackageManagerNative;

import axios from "axios";
import {DOMAIN_SERVER} from "../config";

import DeviceInfo from 'react-native-device-info';
//import JailMonkey from 'jail-monkey';

export default class Confirm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            device: {}
        }
        this.number = '';
    }
    componentWillMount(){
        this.number = this.props.match.params.number;
    }
    componentDidMount(){
        let device = {};
        device.brand = DeviceInfo.getBrand();
        device.buildNumber = DeviceInfo.getBuildNumber();
        device.carrier = DeviceInfo.getCarrier();
        device.deviceCountry = DeviceInfo.getDeviceCountry();
        device.deviceId = DeviceInfo.getDeviceId();
        device.deviceLocale = DeviceInfo.getDeviceLocale();
        device.deviceName = DeviceInfo.getDeviceName();
        device.freeDiskStorage = DeviceInfo.getFreeDiskStorage();
        device.freeDiskStorage = this.readableBytes(device.freeDiskStorage);
        device.manufacturer = DeviceInfo.getManufacturer();
        device.maxMemory = DeviceInfo.getMaxMemory();
        device.maxMemory = this.readableBytes(device.maxMemory);
        device.deviceModel = DeviceInfo.getModel();
        device.storageSize = DeviceInfo.getTotalDiskCapacity();
        device.storageSize = this.readableBytes(device.storageSize);
        device.totalMemory = DeviceInfo.getTotalMemory();
        device.totalMemory = this.readableBytes(device.totalMemory);
        this.setState({device: device});

        /*PackageManagerNative.getPackageInfo()
        .then(info => {
            device.camera_any = info.camera_any === 'yes' ? true : false;
            this.setState({device: device});
        })*/
    }
    componentWillUnmount(){
        this.number = '';
    }
    readableBytes(num) {
        var neg = num < 0;
        var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        if (neg){
            num = -num;
        }

        if (num < 1){
            return (neg ? '-' : '') + num + ' B';
        }

        var exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);

        num = Number((num / Math.pow(1000, exponent)).toFixed(2));

        var unit = units[exponent];
        return (neg ? '-' : '') + num + ' ' + unit;
    }
    renderBooleanBlock(attr, title){
        if(typeof this.state.device[attr] !== 'undefined'){
            return (
                <View style={styles.content}>
                    <View><Text style={styles.title}>{title}</Text></View>
                    <View>
                        {
                            this.state.device[attr]
                            ?
                            <Image source={require('../assets/check.png')} style={{width: 25, height: 25}} />
                            :
                            <Image source={require('../assets/close.png')} style={{width: 25, height: 25}} /> 
                        }
                    </View>
                </View>
            )
        }else return null;
    }
    renderStrBlock(attr, title){
        if(typeof this.state.device[attr] !== 'undefined'){
            return (
                <View style={styles.content}>
                    <View><Text style={styles.title}>{title}</Text></View>
                    <View>
                        <CapitalizedText style={styles.str}>{this.state.device[attr]}</CapitalizedText>
                    </View>
                </View>
            )
        }else return null;
    }
    renderNbBlock(attr, title){
        if(typeof this.state.device[attr] !== 'undefined'){
            return (
                <View style={styles.content}>
                    <View><Text style={styles.title}>{title}</Text></View>
                    <View>
                        <Text style={styles.str}>{this.state.device[attr]}</Text>
                    </View>
                </View>
            )
        }else return null;
    }
    handlerConfirm(){
        axios.post(DOMAIN_SERVER+'transaction/changeStatus', {number: this.number, status: 'checked', device: this.state.device})
        .then((updated) => {
            this.props.history.push('/thanks/'+this.number);
        })
        .catch(() => {
            //this.setState({isLoading: false, isError: false});
        })
    }
    render() {
        let width = Dimensions.get('window').width;
        let height = Dimensions.get('window').height;
        return (
            <ImageBackground source={require('../assets/confirm-bg.jpg')} style={{width: '100%', height: '100%'}}>
                <ScrollView style={{flex:1}}>
                    <View style={{flex: 1, padding: 30, minHeight: height}}>
                        {this.renderStrBlock('brand', 'Brand')}
                        {this.renderNbBlock('buildNumber', 'Build Number')}
                        {this.renderStrBlock('carrier', 'Carrier')}
                        {this.renderStrBlock('deviceCountry', 'Device Country')}
                        {this.renderStrBlock('deviceId', 'Device ID')}
                        {this.renderStrBlock('deviceLocale', 'Device Locale')}
                        {this.renderStrBlock('deviceName', 'Device Name')}
                        {this.renderStrBlock('freeDiskStorage', 'Free Disk Storage')}
                        {this.renderStrBlock('manufacturer', 'Manufacturer')}
                        {this.renderStrBlock('maxMemory', 'Max Memory')}
                        {this.renderStrBlock('deviceModel', 'Model')}
                        {this.renderStrBlock('storageSize', 'Storage Size')}
                        {this.renderStrBlock('totalMemory', 'Total Memory')}
                        {this.renderBooleanBlock('camera_any', 'Has Camera')}
                        <TouchableOpacity style={styles.buttonWrap}
                            onPress={this.handlerConfirm.bind(this)}>
                            <View style={styles.button}>
                                <Text style={styles.buttonText}>CONFIRM</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    padding:30
  },
  content:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  title:{
      fontSize: 18,  
      color:'#fff',
      fontWeight:'bold'
  },
  fixed: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  str: {
    fontSize: 16,
    color:'#fff'
  },
  buttonWrap:{
    flex: 1,
    alignItems: 'center',
  },
  button: {
    width: 260,
    alignItems: 'center',
    backgroundColor: '#ff9800'
  },
  buttonText: {
    padding: 20,
    color: 'black'
  }
});
