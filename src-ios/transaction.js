import * as React from 'react';
import { Text, View, StyleSheet,ImageBackground,TouchableOpacity,TextInput,Image} from 'react-native';
import axios from "axios";
import {DOMAIN_SERVER} from "../config";
import Loading from "./loading";

export default class Transaction extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            transaction: '',
            isLoading: false,
            isError: false
        }
    }
    handlerCheck(){
        this.setState({isLoading: true});
        /*this.setState({isLoading: false, isError: false}, () => {
            this.props.history.push('/confirm/'+this.state.transaction);
        });*/
        if(this.state.transaction){
            axios.post(DOMAIN_SERVER+'transaction/checkNumber', {number: this.state.transaction})
            .then((count) => {
                if(Number(count.data.data) == 0){
                    this.setState({isLoading: false, isError: false}, () => {
                        this.props.history.push('/confirm/'+this.state.transaction);
                    });
                }else
                    this.setState({isLoading: false, isError: true});
            })
            .catch(() => {
                this.setState({isLoading: false, isError: false});
            })
        }else
            this.setState({isLoading: false, isError: true});
    }
    render() {
        return (
        <View>
            <ImageBackground source={require('../assets/transaction-bg.jpg')} style={{width: '100%', height: '100%'}}>
                {/*<View style={{alignItems: 'center',paddingTop:70}}><Image source={require('../assets/logo.png')} style={{width: 70, height: 70}} /></View>*/}
                {
                    this.state.isLoading
                    ?<Loading/>
                    :
                    <View style={styles.wrap}>
                        <Text style={styles.title}>Input Transaction</Text>
                        <TextInput
                            value={this.state.transaction}
                            style={styles.textInput}
                            placeholder="Input Transaction..."
                            placeholderTextColor = "#aaa"
                            onChangeText={(text) => this.setState({transaction: text})}
                        />
                        {
                            this.state.isError
                            ?
                            <Text style={styles.error}>Transaction Number Wrong !!!</Text>
                            : null
                        }
                        <TouchableOpacity onPress={this.handlerCheck.bind(this)}>
                            <View style={styles.button}>
                                <Text style={styles.buttonText}>CHECK YOUR DEVICE</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </ImageBackground>
        
        </View>
        )
    }
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    padding:20
  },
  title:{
    color:'#fff',
    fontSize:17,
    marginBottom: 15
  },
   textInput: {
    justifyContent: "center",
    height: 50,
    width:'100%',
    borderColor: '#fff',
    borderWidth: 1,
    color:'#ddd',
    padding:8
  },
  button: {
    marginTop: 30,
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#ff9800'
  },
  buttonText: {
    padding: 15,
    color: 'black'
  },
  error:{
    color:'red',
    fontSize:13,
    marginTop: 5
  },
 
});
